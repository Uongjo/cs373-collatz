#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve
from Collatz import find_next_thousand, max_cycle_length, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10000 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10000)
        self.assertEqual(j, 999999)

    # ----
    # cycle_length
    # ----

    def test_cycle_length_1(self):
        v = cycle_length(123971)
        self.assertEqual(v, 119)

    def test_cycle_length_2(self):
        v = cycle_length(80085)
        self.assertEqual(v, 33)

    # ----
    # find_next_thousand
    # ----
    def test_find_next_thousand_1(self):
        v = find_next_thousand(300)
        self.assertEqual(v, 1001)

    def test_find_next_thousand_2(self):
        v = find_next_thousand(300200)
        self.assertEqual(v, 301001)

    # ----
    # max_cycle_length
    # ----

    def test_max_cycle_length_1(self):
        v = max_cycle_length(4001, 6000)
        self.assertEqual(v, 236)

    def test_max_cycle_length_2(self):
        v = max_cycle_length(9193, 48844)
        self.assertEqual(v, 324)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(14174, 6505)
        self.assertEqual(v, 276)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    def test_solve_2(self):
        r = StringIO("14174 6505\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "14174 6505 276\n1 1 1\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
