# CS373: Software Engineering Collatz Repo

* Name: Joel Uong

* EID: jtu224

* GitLab ID: uongjo

* HackerRank ID: uongjo

* Git SHA: 4425c5f4bdf8cb3b5754efcbbde45524245fd463

* GitLab Pipelines: https://gitlab.com/Uongjo/cs373-collatz/pipelines

* Estimated completion time: 10 hrs

* Actual completion time: 10 hrs

* Comments: N/A
